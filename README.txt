Dins la carpeta de cada sistema, hi ha:
	- Un script principal (bash per linux i mac, power shell per windows) per fer la release o nomes els instaladors.
	- buildenv: la carpeta on posar-hi el necesari per el zip de build environment. Ara mateix nomes cal el java. El maven s'afageix automaticament.
	- upload-doc-unix.sh: Nomes a Linux i MacOS, script per actualitzar el java-doc. NECESSARI haver fet una release o installers abans. O tenir en aquest directori la carpeta DPFManager amb el mvn install fet.
	
Opcions script principal:
	-i: nomes fara instaladors.
	-nc: no clean, no eliminarÓ re, ni zips ni la carpeta DPFManager. Opcio necessaria per poder actualitzar el java-doc, per no esborrar la carpeta DPFManager.
	
Si no es fa el autoclean (opcio -nc), s'ha de deixar la carpeta com al principi, amb l'script principal, l'script del doc i dins la carpeta buildenv eliminar la carpeta del maven.
	
Pre requisits per poder funcionar els scripts.
	- Java JDK 8.60 o superior. Comprovar amb java -jar
	- El maven NO es necessari, esta inclos aqui.
	- Windows:
		- Inno setup 5
	- Ubuntu: 
		- rpm, rpm-build
		- lftp
	- MacOS:
		- lftp