param(
    [string] $1,
    [string] $2
)

function zipFolder ($source, $target){
    If(Test-path $target) {
	    Remove-item $target
    }

    Add-Type -Assembly System.IO.Compression.FileSystem
    $compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
    [System.IO.Compression.ZipFile]::CreateFromDirectory($source, $target, $compressionLevel, $false)
}

function CreateDirectoriesFromPath ($path, $url, $credentials){
	$DirectoryParts = $path.Split("/");
	$Position = "";
	for ($i = 0; $i -lt $DirectoryParts.Length - 1; $i++){
        try {
	        $Position += $DirectoryParts[$i] + "/";
	        $AbsoluteTemporaryPath = New-Object System.Uri($url+$Position);
	        $WebRequest = [System.Net.WebRequest]::Create($AbsoluteTemporaryPath);
	        $WebRequest.Credentials = $credentials
	        $WebRequest.Method = [System.Net.WebRequestMethods+Ftp]::MakeDirectory;
	        $response = $WebRequest.GetResponse();
	        $response.Close()
        } catch [Net.WebException] { 
	        # Ja existeix la carpeta
        }
	}
}

function sendFtp ($source, $folder, $name){
    #ftp server params
    $ftp = 'ftp://lhcp1017.webapps.net'
    $user = 'preforma@isac.cat'
    $pass = 'ec94E-.,@lW7'
    $credentials = New-Object System.Net.NetworkCredential($user,$pass)
    $target = $ftp+"/"+$folder+"/"+$name

    "Creating sub folders..."
    CreateDirectoriesFromPath -path "/$folder/" -url $ftp -credentials $credentials

    #Connect to ftp webclient
    "Uploading $name..."
    $webclient = New-Object System.Net.WebClient 
    $webclient.Credentials = $credentials
    $uri = New-Object System.Uri($target) 
    $webclient.UploadFile($uri, $source)
}

function getInstallerFile () {
    $files = Get-ChildItem $easyinnova
    for ($i=0; $i -lt $files.Count; $i++) {
        $file = $files[$i]
        $ext = $file.Extension
        if ($ext.Equals(".exe")) {
            return $file
        }
    }
}

function clean () {
    cd $preforma
    rm *.zip
    rm -r DPFManager
    cd buidlenv
    rm -r maven
}

# Constants A DEFINIR
$RELEASE='/dpfmanager/Downloads/Releases/New-Release'
$INSTALLERS='/dpfmanager/Downloads/Future-Release'

# Constants NO MODIFICAR
$preforma = $pwd.Path
cd ..
$baseProj = $pwd.Path
cd $preforma
$maven = "$baseProj/Common/maven"
$dpfmanager = "$preforma/DPFManager"
$easyinnova = "$dpfmanager/target/jfx/native"
$data = Get-Date -format yyyy-MM-dd

# Input params
$isRelease="YES"
$isClean="YES"
$isUpload="NO"
foreach ($arg in $args) {
    if ($arg -eq "-nc") {
        $isClean="NO"
    }
    if ($arg -eq "-i") {
        $isRelease="NO"
    }
	if ($arg -eq "-u") {
        $isUpload="YES"
    }
}

# zip names
$execZip = "$preforma/exec01-$data.zip"
$buildZip = "$preforma/buildenv01-$data.zip"
$srcZip = "$preforma/src01-$data.zip"

# Maven home
$env:M2_HOME = $maven

# Git clone
cd $preforma
git clone https://github.com/EasyinnovaSL/DPFManager.git

# Make src Zip
if ($isRelease -eq "YES") {
    "Zipping src..."
    zipFolder -source "$preforma/DPFManager" -target $srcZip
}

# Run mvn install
cd $dpfmanager
mvn install -DskipTests -Dinstallers

# Get the installer name
$file = getInstallerFile
$fullname = $file.FullName
$parent = $file.DirectoryName
$name = $file.Name

$name2=$name.Substring(5)
$name2="dpf_m$name2"

# Upload installer FTP
if ($isUpload -eq "YES") {
	sendFtp -source $fullname -folder "$INSTALLERS/Windows" -name $name2
}

# zip folders
cd $preforma

# Make exec Zip
if ($isRelease -eq "YES") {
    "Zipping exec..."
    zipFolder -source "$parent" -target $execZip
}

# Make buildenv Zip
if ($isRelease -eq "YES") {
    "Zipping buildenv..."
    Copy-Item $maven "$preforma/buildenv" -recurse
    zipFolder -source "$preforma/buildenv" -target $buildZip
}

# Upload zips
$isRelease="NO"
if ($isUpload -eq "YES") {
	if ($isRelease -eq "YES") {
		sendFtp -source $execZip -folder "$RELEASE/Windows" -name "exec01-$data.zip"
		sendFtp -source $buildZip -folder "$RELEASE/Windows" -name "buildenv01-$data.zip"
		sendFtp -source $srcZip -folder "$RELEASE/Windows" -name "src01-$data.zip"
	}
}

# Clean project
if ($isClean -eq "YES") {
    clean
}
