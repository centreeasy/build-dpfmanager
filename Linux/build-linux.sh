#!/bin/bash

function zipSrc {
	zip ../src.zip -r *
}

function zipExec {
	cd $easyinnova
	zip "$preforma/deb.zip" *.deb
	zip "$preforma/rpm.zip" *.rpm
	cd $easyinnovaJava
	zip "$preforma/deb.zip" *.deb
	zip "$preforma/rpm.zip" *.rpm
}

function zipBuildenv {
	cd $common
	cp -r maven $buildenv
	cd $buildenv
	zip build.zip *
	mv *.zip $preforma
}

function uploadZips {
	initRelease
	upload "Ubuntu" "09"
	upload "Fedora" "13"
	upload "Debian" "17"
	upload "OpenSuse" "21"
	finishRelease
}

function initRelease {
	# Init commands release file
	echo "set ssl:verify-certificate no" > $commands
	echo "cd $RELEASE" >> $commands
	echo "mkdir $RELEASE_FOLDER" >> $commands
}

function upload {
	name=$1
	number=$2
	current=$(pwd)
	echo "cd $RELEASE/$RELEASE_FOLDER" >> $commands
	echo "mkdir $name" >> $commands
	echo "cd $name" >> $commands
	# Build env
	local="$current/build.zip"
	remote="buildenv$number-$now.zip"
	echo "put $local -o $remote" >> $commands
	# Build env
	local="$current/src.zip"
	remote="src$number-$now.zip"
	echo "put $local -o $remote" >> $commands
	# Exec
	if [ $number = "09" ] || [ $number = "17" ]; then
		local="$current/deb.zip"
	else
		local="$current/rpm.zip"
	fi
	remote="exec$number-$now.zip"
	echo "put $local -o $remote" >> $commands
}

function finishRelease {
	echo "bye" >> $commands
	# Upload it
	lftp -u $USER,$PASS $URL < $commands
}

function uploadInstallers {
	initInstallers
	uploadInstaller "Ubuntu" "09" $1
	uploadInstaller "Fedora" "13" $1
	uploadInstaller "Debian" "17" $1
	uploadInstaller "OpenSuse" "21" $1
	finishInstallers	
}

function initInstallers {
	# Init commands installers file
	echo "set ssl:verify-certificate no" > $commands0
	echo "cd $INSTALLERS" >> $commands0
	echo "mkdir $INSTALLERS_FOLDER" >> $commands0
}

function uploadInstaller {
	name=$1
	number=$2
	type=$3
	current=$(pwd)
	
	echo "cd $INSTALLERS/$INSTALLERS_FOLDER" >> $commands0
	echo "mkdir $name" >> $commands0
	echo "cd $name" >> $commands0
	
	search="rpm"
	if [ $number = "09" ] || [ $number = "17" ]; then
		search="deb"
	fi
	
	for file in *.$search; do
		local="$current/$file"
		remote=$file
		if [ $type = "java" ]; then
			remote="java-$file"
		fi
		echo "put $local -o $remote" >> $commands0
	done
}

function finishInstallers {
	echo "bye" >> $commands0
	# Upload it
	lftp -u $USER,$PASS $URL < $commands0
}

function clean {
	# Clean folder
	cd $preforma
	rm *.zip
	rm $commands
	rm $commands0
	rm -r DPFManager/
	cd $buildenv
	rm -r maven
}

# Constants A DEFINIR
RELEASE='/dpfmanager/Downloads/Releases'
RELEASE_FOLDER='New-Release'
INSTALLERS='/dpfmanager/Downloads'
INSTALLERS_FOLDER='Future-Release'

# Credencials
URL='lhcp1017.webapps.net'
USER='preforma@isac.cat'
PASS='ec94E-.,@lW7'

# Constants NO MODIFICAR
preforma=$(pwd)
cd ..
baseproj=$(pwd)
common="$baseproj/Common/"
buildenv="$preforma/buildenv/"
DPFManager="$preforma/DPFManager"
easyinnova="$DPFManager/target/jfx/native"
easyinnovaJava="$DPFManager/target/jfx/java"
now="$(date +'%Y-%m-%d')"
commands="$preforma/commands-release.txt"
commands0="$preforma/commands-installers.txt"

# Input commands
isRelease="YES"
if [ $1 = "-i" ] || [ $2 = "-i" ] || [ $3 = "-i" ]; then
	isRelease="NO"
fi
isClean="YES"
if [ $1 = "-nc" ] || [ $2 = "-nc" ] || [ $3 = "-nc" ]; then
	isClean="NO"
fi
isUpload="NO"
if [ $1 = "-u" ] || [ $2 = "-u" ] || [ $3 = "-u" ]; then
	isUpload="YES"
fi

# Maven home
export M2_HOME="$common/maven"
export PATH="$PATH:$M2_HOME/bin"

# Get from git
cd $preforma
git clone https://github.com/EasyinnovaSL/DPFManager.git DPFManager

# Create src zip
cd $DPFManager
if [ $isRelease = "YES" ]; then
	zipSrc
fi

# Run mvn install
cd $DPFManager
mvn install -DskipTests

# Now upload installers
if [ $isUpload = "YES" ]; then
	cd $easyinnova
	uploadInstallers "native"
	cd $easyinnovaJava
	uploadInstallers "java"
fi

# Create exec zip
if [ $isRelease = "YES" ]; then
	zipExec
fi

# Create buildenv zip
cd "$preforma"
if [ $isRelease = "YES" ]; then
	zipBuildenv
fi

# Upload all zips
cd $preforma
if [ $isRelease = "YES" ] && [ $isUpload = "YES" ]; then
	uploadZips
fi

# Clean project
if [ $isClean = "YES" ]; then
	clean
fi
