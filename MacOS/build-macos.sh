#!/bin/bash

function zipSrc {
	zip ../src.zip -r *
}

function zipExec {
	zip dmg.zip *.dmg
	mv *.zip $preforma
}

function zipBuildenv {
	cd $common
	cp -r maven $buildenv
	cd $buildenv
	zip build.zip *
	mv *.zip $preforma
}

function uploadZips {
	initRelease
	upload "MacOS" "05"
	finishRelease
}

function initRelease {
	# Init commands release file
	echo "set ssl:verify-certificate no" > $commands
	echo "cd $RELEASE" >> $commands
	echo "mkdir $RELEASE_FOLDER" >> $commands
}

function upload {
	name=$1
	number=$2
	current=$(pwd)
	echo "cd $RELEASE/$RELEASE_FOLDER" >> $commands
	echo "mkdir $name" >> $commands
	echo "cd $name" >> $commands
	# Build env
	local="$current/build.zip"
	remote="buildenv$number-$now.zip"
	echo "put $local -o $remote" >> $commands
	# Build env
	local="$current/src.zip"
	remote="src$number-$now.zip"
	echo "put $local -o $remote" >> $commands
	# Exec
	local="$current/dmg.zip"
	remote="exec$number-$now.zip"
	echo "put $local -o $remote" >> $commands
}

function finishRelease {
	echo "bye" >> $commands
	# Upload it
	lftp -u $USER,$PASS $URL < $commands
}

function uploadInstallers {
	renameInstaller
	initInstallers
	uploadInstaller "MacOS" "05"
	finishInstallers	
}

function renameInstaller {
	current=$(pwd)
	search="dmg"
	for file in *.$search; do
	echo "File: $file"
		renameLocalFile "$file"
	done
}

function renameLocalFile {
	name=$1
	begin="DPF M"
	replace="dpf_m"
	if [ "${name:0:${#begin}}" == "$begin" ]; then
		newName="$replace${name:5:${#name}}"
		mv "$name" "$newName"
	fi
}

function initInstallers {
	# Init commands installers file
	echo "set ssl:verify-certificate no" > $commands0
	echo "cd $INSTALLERS" >> $commands0
	echo "mkdir $INSTALLERS_FOLDER" >> $commands0
}

function uploadInstaller {
	name=$1
	number=$2
	current=$(pwd)
	
	echo "cd $INSTALLERS/$INSTALLERS_FOLDER" >> $commands0
	echo "mkdir $name" >> $commands0
	echo "cd $name" >> $commands0
	
	search="dmg"
	for file in *.$search; do
		local="$current/$file"
		remote=$file
		echo "put $local -o $remote" >> $commands0
	done
}

function finishInstallers {
	echo "bye" >> $commands0
	# Upload it
	lftp -u $USER,$PASS $URL < $commands0
}

function clean {
	# Clean folder
	cd $preforma
	rm *.zip
	rm $commands
	rm $commands0
	rm -r DPFManager/
	cd $buildenv
	rm -r maven
}

# Constants A DEFINIR
RELEASE='/dpfmanager/Downloads/Releases'
RELEASE_FOLDER='New-Release'
INSTALLERS='/dpfmanager/Downloads'
INSTALLERS_FOLDER='Future-Release'

# Credencials
URL='lhcp1017.webapps.net'
USER='preforma@isac.cat'
PASS='ec94E-.,@lW7'

# Constants NO MODIFICAR
preforma=$(pwd)
cd ..
baseproj=$(pwd)
common="$baseproj/Common/"
buildenv="$preforma/buildenv/"
DPFManager="$preforma/DPFManager"
easyinnova="$DPFManager/target/jfx/native"
now="$(date +'%Y-%m-%d')"
commands="$preforma/commands-release.txt"
commands0="$preforma/commands-installers.txt"

# Input commands
isRelease="YES"
if [ $1 = "-i" ] || [ $2 = "-i" ]; then
	isRelease="NO"
fi
isClean="YES"
if [ $1 = "-nc" ] || [ $2 = "-nc" ]; then
	isClean="NO"
fi

# Maven home
export M2_HOME="$common/maven"
export PATH="$PATH:$M2_HOME/bin"

# Get from git
cd $preforma
git clone https://github.com/EasyinnovaSL/DPFManager.git DPFManager

# Create src zip
cd $DPFManager
if [ $isRelease = "YES" ]; then
	zipSrc
fi

# Run mvn install
cd $DPFManager
mvn install -DskipTests

# Now upload installers
cd $easyinnova
uploadInstallers

# Create exec zip
cd $easyinnova
if [ $isRelease = "YES" ]; then
	zipExec
fi

# Create buildenv zip
cd "$preforma"
if [ $isRelease = "YES" ]; then
	zipBuildenv
fi

# Upload all zips
cd $preforma
if [ $isRelease = "YES" ]; then
	uploadZips
fi

# Clean project
if [ $isClean = "YES" ]; then
	clean
fi
